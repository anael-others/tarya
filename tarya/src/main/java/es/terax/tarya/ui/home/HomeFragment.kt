/*
 * Tarya performs fast numerological calculus.
 *
 *  Copyright (C) 2022 Anael González Paz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package es.terax.tarya.ui.home

import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.icu.text.SimpleDateFormat
import android.icu.util.Calendar
import android.icu.util.TimeZone
import android.os.Bundle
import android.view.*
import android.widget.AutoCompleteTextView
import android.widget.Button
import android.widget.CheckBox
import android.widget.CompoundButton
import androidx.core.view.isVisible
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.textfield.MaterialAutoCompleteTextView
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import es.terax.tarya.DropDownAdapter
import es.terax.tarya.R
import es.terax.tarya.databinding.FragmentHomeBinding
import java.time.LocalDate
import java.time.format.DateTimeFormatter


class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        val buttonCalculate: Button = binding.buttonCalculate
        val typeLayout: TextInputLayout = binding.menu
        val typeInput: AutoCompleteTextView = binding.autoCompleteType
        val textInput: TextInputEditText = binding.textInput
        val textSurname: TextInputEditText = binding.input
        val textInputSurname: TextInputLayout = binding.textInputLayout
        val dateInput: TextInputEditText = binding.inputDate
        val editTextDate: TextInputLayout = binding.editTextDate
        val editTextActive: TextInputLayout = binding.editTextActiveName
        val textActiveInput: TextInputEditText = binding.textActiveInput
        val checkBox: CheckBox = binding.checkBox
        val referenceDateInput: TextInputEditText = binding.inputReferenceDate
        val editTextReferenceDate: TextInputLayout = binding.editTextReferenceDate
        buttonCalculate.setOnClickListener{calculate()}
        textInput.doOnTextChanged { text, start, before, count -> validateInput(
            0, text, start, before, count)}
        textSurname.doOnTextChanged { text, start, before, count -> validateInput(
            1, text, start, before, count)}
        dateInput.doOnTextChanged{ text, start, before, count -> validateInput(
            2, text, start, before, count
        )}
        textActiveInput.doOnTextChanged { text, start, before, count -> validateInput(
            3, text, start, before, count
        )}
        dateInput.setOnClickListener{
            showDatePickerDialog()
        }
        referenceDateInput.setOnClickListener {
            showReferenceDatePickerDialog()
        }
        typeInput.doOnTextChanged { text, start, before, count ->
            changeMode(text, start, before, count)
        }
        checkBox.setOnCheckedChangeListener { compoundButton, b ->
            onCheckBoxCheckedChanged(compoundButton, b)
        }
        textInputSurname.isVisible = false
        editTextDate.isVisible = false
        editTextActive.isVisible = false
        checkBox.isVisible = false
        editTextReferenceDate.isVisible = false
        val format = SimpleDateFormat("dd/MM/yyyy")
        referenceDateInput.setText(format.format(MaterialDatePicker.todayInUtcMilliseconds()))

        val items = arrayOf(getString(R.string.array_general),getString(R.string.array_person),getString(R.string.array_dates))
        val adapter = DropDownAdapter(requireContext(), items)
        (typeLayout.editText as? MaterialAutoCompleteTextView)?.setAdapter(adapter)
        typeInput.setText(getString(R.string.array_general), false)

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
            binding.topAppBar.menu.setGroupVisible(1, true)
        }
        binding.topAppBar.setOnMenuItemClickListener { menuItem ->
            when (menuItem.itemId)
            {
                R.id.about -> {
                    val actionAbout = HomeFragmentDirections.actionNavigationHomeToAboutFragment()
                    binding.root.findNavController().navigate(actionAbout)
                    true
                }
                else -> false
            }
        }
        return binding.root
    }

    private fun showDatePickerDialog() {
        binding.editTextDate.isEnabled = false
        val dateInput: TextInputEditText = binding.inputDate
        val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setSelection(if (dateInput.text.toString().isEmpty())
            {
                MaterialDatePicker.todayInUtcMilliseconds()
            }
            else
            {
                simpleDateFormat.parse(dateInput.text.toString()).time
            })
            .build()
        val fragmentManager = (requireContext() as FragmentActivity).supportFragmentManager
        datePicker.addOnPositiveButtonClickListener {
            val utc: Calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            utc.timeInMillis = it
            val format = SimpleDateFormat("dd/MM/yyyy")
            val formatted: String = format.format(utc.time)
            dateInput.setText(formatted)
        }
        datePicker.addOnNegativeButtonClickListener {
            binding.editTextDate.isEnabled = true
        }
        datePicker.addOnCancelListener {
            binding.editTextDate.isEnabled = true
        }
        datePicker.addOnDismissListener {
            binding.editTextDate.isEnabled = true
        }
        datePicker.show(fragmentManager, "DatePickerDialog")
    }

    private fun showReferenceDatePickerDialog() {
        binding.editTextReferenceDate.isEnabled = false
        val dateInput: TextInputEditText = binding.inputReferenceDate
        val simpleDateFormat = SimpleDateFormat("dd/MM/yyyy")
        simpleDateFormat.timeZone = TimeZone.getTimeZone("UTC")
        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setSelection(if (dateInput.text.toString().isEmpty())
            {
                MaterialDatePicker.todayInUtcMilliseconds()
            }
            else
            {
                simpleDateFormat.parse(dateInput.text.toString()).time
            })
            .build()
        val fragmentManager = (requireContext() as FragmentActivity).supportFragmentManager
        datePicker.addOnPositiveButtonClickListener {
            val utc: Calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
            utc.timeInMillis = it
            val format = SimpleDateFormat("dd/MM/yyyy")
            val formatted: String = format.format(utc.time)
            dateInput.setText(formatted)
        }
        datePicker.addOnNegativeButtonClickListener {
            binding.editTextReferenceDate.isEnabled = true
        }
        datePicker.addOnCancelListener {
            binding.editTextReferenceDate.isEnabled = true
        }
        datePicker.addOnDismissListener {
            binding.editTextReferenceDate.isEnabled = true
        }
        datePicker.show(fragmentManager, "ReferenceDatePickerDialog")
    }

    private fun calculate() {
        val textInput: String = binding.textInput.text.toString()
        val textViewName: TextInputLayout = binding.editTextName
        val typeInput: String= binding.autoCompleteType.text.toString()
        val textInputSurname: String = binding.input.text.toString()
        val textInputSurnameLayout: TextInputLayout = binding.textInputLayout
        val editTextDate: TextInputLayout = binding.editTextDate
        val valueDate: String = binding.inputDate.text.toString()
        val valueReferenceDate: String = binding.inputReferenceDate.text.toString()
        val editTextReferenceDate: TextInputLayout = binding.editTextReferenceDate
        val textActive: String = binding.textActiveInput.text.toString()
        val textActiveLayout: TextInputLayout = binding.textInputLayout
        val checkBox: CheckBox = binding.checkBox
        var isErroneous = false
        if (typeInput == getString(R.string.array_person)) {
            if (textInput.isEmpty()) {
                textViewName.error = getString(R.string.main_empty)
                isErroneous = true
            }
            else {
                textViewName.error = null
            }
            if (textInputSurname.isEmpty()) {
                textInputSurnameLayout.error = getString(R.string.main_empty)
                isErroneous = true
            }
            else {
                textInputSurnameLayout.error = null
            }
            if (valueDate.isEmpty()) {
                editTextDate.error = getString(R.string.main_empty)
                isErroneous = true
            }
            else {
                editTextDate.error = null
            }
            if (textActive.isEmpty() && checkBox.isChecked) {
                textActiveLayout.error = getString(R.string.main_empty)
                isErroneous = true
            }
            else {
                textActiveLayout.error = null
            }
            if (!isErroneous) {
                val localDate = LocalDate.parse(valueDate,
                    DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                val todayDate: LocalDate = LocalDate.now()
                // Process data accordingly.
                val valuePath: Int = intReduction(
                    localDate.dayOfMonth + localDate.monthValue + localDate.year)
                val valuePersonality: Int = intReduction(processStrings(
                    textInput.uppercase())+processStrings(textInputSurname.uppercase()))
                val dateBase: Int = localDate.dayOfMonth + localDate.monthValue
                val preValueYear: Int = if (localDate.monthValue < todayDate.monthValue) {
                    dateBase + todayDate.year
                } else if (localDate.monthValue == todayDate.monthValue) {
                    if (localDate.dayOfMonth <= todayDate.dayOfMonth) {
                        dateBase + todayDate.year
                    } else {
                        dateBase + todayDate.year - 1
                    }
                } else {
                    dateBase + todayDate.year - 1
                }
                val passingName: String
                val valueName: Int = if (checkBox.isChecked) {
                    passingName = textActive
                    intReduction(processStrings(textActive.uppercase()))
                }
                else {
                    passingName = textInput
                    intReduction(processStrings(textInput.uppercase()))
                }
                var newPassingName = ""
                if (passingName.length > 20) {
                    for (i in 0..20) {
                        newPassingName += passingName[i]
                    }
                    newPassingName += "…"
                }
                else {
                    newPassingName = passingName
                }
                val actionResults = HomeFragmentDirections.actionNavigationHomeToMyResultsFragment(
                    valuePath,
                    valuePersonality,
                    intReduction(valuePath+valuePersonality),
                    intReduction(processVowelStrings(
                        textInput.uppercase() + textInputSurname.uppercase())),
                    intReduction(processConsonantStrings(
                        textInput.uppercase() + textInputSurname.uppercase())),
                    valueName,
                    intReduction(preValueYear),
                    newPassingName)
                binding.root.findNavController().navigate(actionResults)
            }
        }
        else if (typeInput == getString(R.string.array_general))
        {
            if (textInput.isEmpty()) {
                textViewName.error = getString(R.string.main_empty)
                isErroneous = true
            }
            else {
                textViewName.error = null
            }
            if (!isErroneous) {
                // Process data accordingly.
                var newPassingName = ""
                if (textInput.length > 20) {
                    for (i in 0..20) {
                        newPassingName += textInput[i]
                    }
                    newPassingName += "…"
                }
                else {
                    newPassingName = textInput
                }
                val actionResults = HomeFragmentDirections.actionNavigationHomeToNavigationResults(
                    intReduction(processStrings(textInput.uppercase())),
                    intReduction(processVowelStrings(textInput.uppercase())),
                    intReduction(processConsonantStrings(textInput.uppercase())),
                    false,
                    newPassingName)
                binding.root.findNavController().navigate(actionResults)
            }
        }
        else
        {
            if (valueDate.isEmpty()) {
                editTextDate.error = getString(R.string.main_empty)
                isErroneous = true
            }
            else {
                editTextDate.error = null
            }
            if (valueReferenceDate.isEmpty()) {
                editTextReferenceDate.error = getString(R.string.main_empty)
                isErroneous = true
            }
            else {
                editTextReferenceDate.error = null
            }
            if (!isErroneous) {
                val sharedPref: SharedPreferences = context?.getSharedPreferences(
                    getString(R.string.app_preferences_file_key), MODE_PRIVATE) ?: return
                with (sharedPref.edit()) {
                    putString("userBirthday", valueDate)
                    apply()
                }
                val localDate = LocalDate.parse(valueDate,
                    DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                val todayDate = LocalDate.parse(valueReferenceDate,
                    DateTimeFormatter.ofPattern("dd/MM/yyyy"))
                // Process data accordingly.
                val dateBase: Int = localDate.dayOfMonth + localDate.monthValue
                val preValueYear: Int = if (localDate.monthValue < todayDate.monthValue) {
                    dateBase + todayDate.year
                } else if (localDate.monthValue == todayDate.monthValue) {
                    if (localDate.dayOfMonth <= todayDate.dayOfMonth) {
                        dateBase + todayDate.year
                    } else {
                        dateBase + todayDate.year - 1
                    }
                } else {
                    dateBase + todayDate.year - 1
                }
                val valueYear = intReduction(preValueYear)
                val valueMonth = intReduction(valueYear + todayDate.monthValue)
                val valueDay = intReduction(
                    valueYear + todayDate.monthValue + todayDate.dayOfMonth)
                val actionResults = HomeFragmentDirections.actionNavigationHomeToNavigationResults(
                    valueYear,
                    valueMonth,
                    valueDay,
                    true
                )
                binding.root.findNavController().navigate(actionResults)
            }
        }
    }

    private fun validateInput(mode: Int, text: CharSequence?,
                              @Suppress("UNUSED_PARAMETER") _start: Int,
                              @Suppress("UNUSED_PARAMETER") _before: Int,
                              @Suppress("UNUSED_PARAMETER") _count: Int) {
        val textViewName: TextInputLayout = when (mode) {
            0 -> binding.editTextName
            1 -> binding.textInputLayout
            2 -> binding.editTextDate
            else -> binding.editTextActiveName
        }
         if (text.toString().isNotEmpty()) {
            textViewName.error = null
        }
    }
    private fun processStrings(dataInput: String): Int {
        var returnValue = 0
        for (i in dataInput.indices) {
            if (dataInput[i] == 'A' || dataInput[i] == 'Á' || dataInput[i] == 'J' ||
                dataInput[i] == 'S') {
                returnValue++
            }
            else if (dataInput[i] == 'B' || dataInput[i] == 'K' || dataInput[i] == 'T') {
                returnValue += 2
            }
            else if (dataInput[i] == 'C' || dataInput[i] == 'L' || dataInput[i] == 'U' ||
                dataInput[i] == 'Ú') {
                returnValue += 3
            }
            else if (dataInput[i] == 'D' || dataInput[i] == 'M' || dataInput[i] == 'V') {
                returnValue += 4
            }
            else if (dataInput[i] == 'E' || dataInput[i] == 'É' || dataInput[i] == 'N' ||
                dataInput[i] == 'Ñ' || dataInput[i] == 'W') {
                returnValue += 5
            }
            else if (dataInput[i] == 'F' || dataInput[i] == 'O' || dataInput[i] == 'Ó' ||
                dataInput[i] == 'X') {
                returnValue += 6
            }
            else if (dataInput[i] == 'G' || dataInput[i] == 'P' || dataInput[i] == 'Y') {
                returnValue += 7
            }
            else if (dataInput[i] == 'H' || dataInput[i] == 'Q' || dataInput[i] == 'Z') {
                returnValue += 8
            }
            else if (dataInput[i] == 'I' || dataInput[i] == 'Í' || dataInput[i] == 'R') {
                returnValue += 9
            }
        }
        return returnValue
    }
    private fun processVowelStrings(dataInput: String): Int {
        var returnValue = 0
        for (i in dataInput.indices) {
            if (dataInput[i] == 'A' || dataInput[i] == 'Á') {
                returnValue++
            }
            else if (dataInput[i] == 'U' || dataInput[i] == 'Ú') {
                returnValue += 3
            }
            else if (dataInput[i] == 'E' || dataInput[i] == 'É') {
                returnValue += 5
            }
            else if (dataInput[i] == 'O' || dataInput[i] == 'Ó') {
                returnValue += 6
            }
            else if (dataInput[i] == 'I' || dataInput[i] == 'Í') {
                returnValue += 9
            }
        }
        return returnValue
    }
    private fun processConsonantStrings(dataInput: String): Int {
        var returnValue = 0
        for (i in dataInput.indices) {
            if (dataInput[i] == 'J' || dataInput[i] == 'S') {
                returnValue++
            }
            else if (dataInput[i] == 'B' || dataInput[i] == 'K' || dataInput[i] == 'T') {
                returnValue += 2
            }
            else if (dataInput[i] == 'C' || dataInput[i] == 'L') {
                returnValue += 3
            }
            else if (dataInput[i] == 'D' || dataInput[i] == 'M' || dataInput[i] == 'V') {
                returnValue += 4
            }
            else if (dataInput[i] == 'N' || dataInput[i] == 'Ñ' || dataInput[i] == 'W') {
                returnValue += 5
            }
            else if (dataInput[i] == 'F' || dataInput[i] == 'X') {
                returnValue += 6
            }
            else if (dataInput[i] == 'G' || dataInput[i] == 'P' || dataInput[i] == 'Y') {
                returnValue += 7
            }
            else if (dataInput[i] == 'H' || dataInput[i] == 'Q' || dataInput[i] == 'Z') {
                returnValue += 8
            }
            else if (dataInput[i] == 'R') {
                returnValue += 9
            }
        }
        return returnValue
    }
    private fun intReduction(currentBottle: Int): Int {
        var dataInput = currentBottle
        var returnValue = 0
        while (true) {
            if (dataInput == 11 || dataInput == 22 || dataInput == 33 || dataInput == 44) {
                returnValue = dataInput
                break
            }
            while (dataInput != 0) {
                returnValue += dataInput % 10
                dataInput /= 10
            }
            if (returnValue > 9) {
                if (returnValue == 11 || returnValue == 22 || returnValue == 33 ||
                    returnValue == 44) {
                    break
                }
                else {
                    dataInput = returnValue
                    returnValue = 0
                }
            }
            else {
                break
            }
        }
        return returnValue
    }

    private fun changeMode(@Suppress("UNUSED_PARAMETER") text: CharSequence?,
                   @Suppress("UNUSED_PARAMETER") _start: Int,
                   @Suppress("UNUSED_PARAMETER") _before: Int,
                   @Suppress("UNUSED_PARAMETER") _count: Int) {
        val nameInput: TextInputLayout = binding.editTextName
        val typeInput: AutoCompleteTextView = binding.autoCompleteType
        val textInputSurname: TextInputLayout = binding.textInputLayout
        val editTextDate: TextInputLayout = binding.editTextDate
        val editTextActive: TextInputLayout = binding.editTextActiveName
        val checkBox: CheckBox = binding.checkBox
        val editTextReferenceDate: TextInputLayout = binding.editTextReferenceDate
        if (typeInput.text.toString() == getString(R.string.array_person)) {
            nameInput.isVisible = true
            textInputSurname.isVisible = true
            editTextDate.isVisible = true
            editTextActive.isVisible = checkBox.isChecked
            checkBox.isVisible = true
            editTextReferenceDate.isVisible = false
        }
        else if (typeInput.text.toString() == getString(R.string.array_general)) {
            nameInput.isVisible = true
            textInputSurname.isVisible = false
            editTextDate.isVisible = false
            editTextActive.isVisible = false
            checkBox.isVisible = false
            editTextReferenceDate.isVisible = false
        }
        else {
            nameInput.isVisible = false
            textInputSurname.isVisible = false
            editTextDate.isVisible = true
            val sharedPref: SharedPreferences = context?.getSharedPreferences(
                getString(R.string.app_preferences_file_key), MODE_PRIVATE) ?: return
            if (sharedPref.getString("userBirthday", null) != null) {
                binding.inputDate.setText(sharedPref.getString("userBirthday", null))
            }
            editTextActive.isVisible = false
            checkBox.isVisible = false
            editTextReferenceDate.isVisible = true
        }
    }

    private fun onCheckBoxCheckedChanged(
        @Suppress("UNUSED_PARAMETER") compoundButton: CompoundButton, isChecked: Boolean) {
        val editTextActive: TextInputLayout = binding.editTextActiveName
        editTextActive.isVisible = isChecked
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}