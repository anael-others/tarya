/*
 * Tarya performs fast numerological calculus.
 *
 *  Copyright (C) 2022 Anael González Paz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package es.terax.tarya.ui.results

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import es.terax.tarya.R
import es.terax.tarya.databinding.FragmentResultsBinding

class ResultsFragment : Fragment() {

    private var _binding: FragmentResultsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val args: ResultsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        //val dashboardViewModel =
        //    ViewModelProvider(this).get(ResultsViewModel::class.java)

        _binding = FragmentResultsBinding.inflate(inflater, container, false)
        //val root: View = binding.root

        val textView: TextView = binding.labelGeneral
        val labelVowels: TextView = binding.labelVowels
        val labelConsonants: TextView = binding.labelConsonants
        val textGeneral: TextView = binding.textGeneral
        val textVowels: TextView = binding.textVowels
        val textConsonants: TextView = binding.textConsonants

        if (args.variableMode) {
            textGeneral.text = getString(R.string.results_year)
            textVowels.text = getString(R.string.results_month)
            textConsonants.text = getString(R.string.results_day)
            binding.topAppBar.subtitle = getString(R.string.array_dates)
        }
        else {
            binding.topAppBar.subtitle = args.textValue
        }
        textView.text = args.textInput.toString()
        labelVowels.text = args.valueVowels.toString()
        labelConsonants.text = args.valueConsonants.toString()

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
            binding.topAppBar.menu.setGroupVisible(1, true)
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}