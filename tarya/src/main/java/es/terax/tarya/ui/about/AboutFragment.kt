/*
 * Tarya performs fast numerological calculus.
 *
 *  Copyright (C) 2022 Anael González Paz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package es.terax.tarya.ui.about

import android.content.Intent
import android.graphics.text.LineBreaker.JUSTIFICATION_MODE_INTER_WORD
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import es.terax.tarya.R
import es.terax.tarya.databinding.FragmentAboutBinding

class AboutFragment: Fragment() {
    private var _binding: FragmentAboutBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAboutBinding.inflate(inflater, container, false)

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
            binding.topAppBar.menu.setGroupVisible(1, true)
        }
        val labelVersion: TextView = binding.labelVersion
        try {
            val pInfo = context!!.packageManager.getPackageInfo(context!!.packageName, 0)
            val version: String = pInfo.versionName
            labelVersion.text = getString(R.string.about_version, version)
        }catch (e:Exception){
            e.printStackTrace()
        }
        val labelCopyright: TextView = binding.labelCopyright
        labelCopyright.text = getString(R.string.about_copyright, "2022")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            binding.labelLicense.justificationMode = JUSTIFICATION_MODE_INTER_WORD
            binding.labelLicenseText.justificationMode = JUSTIFICATION_MODE_INTER_WORD
        }
        binding.floatingActionButton.setOnClickListener {
            visitURL("https://gitlab.com/anael-others/tarya")
        }
        binding.button.setOnClickListener {
            visitURL("https://www.gnu.org/licenses/gpl-3.0.en.html")
        }
        return binding.root
    }

    private fun visitURL(uriString: String) {
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uriString))
        if (intent.resolveActivity(context!!.packageManager) != null) {
            startActivity(intent)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}