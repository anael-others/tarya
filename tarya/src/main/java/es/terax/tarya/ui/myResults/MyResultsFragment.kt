/*
 * Tarya performs fast numerological calculus.
 *
 *  Copyright (C) 2022 Anael González Paz
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package es.terax.tarya.ui.myResults

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import es.terax.tarya.databinding.FragmentMyResultsBinding

class MyResultsFragment : Fragment() {

    private var _binding: FragmentMyResultsBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val args: MyResultsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMyResultsBinding.inflate(inflater, container, false)

        val labelPath: TextView = binding.labelPath
        val labelPersonality: TextView = binding.labelPersonality
        val labelPotential: TextView = binding.labelPotential
        val labelVowels: TextView = binding.labelVowels2
        val labelConsonants: TextView = binding.labelConsonants2
        val labelActive: TextView = binding.labelActive
        val labelYear: TextView = binding.labelYear

        labelPath.text = args.valuePath.toString()
        labelPersonality.text = args.valuePersonality.toString()
        labelPotential.text = args.valuePotential.toString()
        labelVowels.text = args.valueVowels.toString()
        labelConsonants.text = args.valueConsonants.toString()
        labelActive.text = args.valueActive.toString()
        labelYear.text = args.valueYear.toString()
        binding.topAppBar.subtitle = args.valueName

        binding.topAppBar.setNavigationOnClickListener {
            findNavController().navigateUp()
            binding.topAppBar.menu.setGroupVisible(1, true)
        }

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}