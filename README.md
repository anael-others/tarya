# Tarya
Fast numerological calculus

## Description
This application provides a fast, Android oriented way of performing calculus in numerology.

## DISCLAIMER
This project was requested by the same people who asked me for the
[Numerología Cuántica](https://gitlab.com/anael-others/numerologia-cuantica) project,
so they do both share the same develop conditions. (Listed in the 'Support and contributing' section).    
The fact that I had developed this program does **NOT** imply, by any means, that I believe on the
use case of this application, since I was requested by a third-party group to code it.  
Part of the conditions in order to made this program were to license it under a
[free license](https://gitlab.com/anael-others/numerologia-cuantica/-/blob/main/COPYING).

## Support and contributing
Since it was requested by a third-party group, *I will* **NOT** *PROVIDE SUPPORT* to this application,
other than the one that I may provide to that specific group of people,whose requests I would try to fulfill.  
This repository, as well as the Launchpad one is subject to be *deleted* at any moment.

Anyways, I will still maintain this repo from time to time, improve the existing code, or add new features. If you would like to ask for more features, just open an Issue and, if the destination users liked it, I would try to implement it on my spare time.

You are free to fork this repo, and make any modifications, as long as they follow this [project License](https://www.gnu.org/licenses/gpl-3.0.en.html).

## License
This project is licensed under the third version of the GNU General Public License (GPL-3.0).  
[![GNU GPL-3.0](https://www.gnu.org/graphics/gplv3-127x51.png)](https://www.gnu.org/licenses/gpl-3.0.en.html)